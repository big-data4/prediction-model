import boto3
import csv
import time
import datetime
import os
from prediction import jobPredictor

sqs = boto3.resource('sqs')
s3 = boto3.resource('s3')
bucket = s3.Bucket('big-data-bucket')
request_queue = sqs.get_queue_by_name(QueueName='big-data-request')
response_queue = sqs.get_queue_by_name(QueueName='big-data-response')

def wait_for_response(queue):
    print("Waiting for response...")
    output = "?"
    while output == "?":
        time.sleep(5)
        messages = queue.receive_messages(
            MaxNumberOfMessages=1,
            MessageAttributeNames=['All']
        )

        for message in messages:
            output = message.body
            message.delete()
            break

    return output

while(True):
    output = wait_for_response(request_queue)
    print('Downloading from bucket...')
    bucket.download_file(output, "./request.csv")
    data_list = []
    print('Reading CSV...')
    with open('./request.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='|')
        for row in spamreader:
            jobId = jobPredictor(row[0])
            data_list.append([row[0], jobId[0]])
    print('Creating response...')
    with open('./predict.csv', 'w', newline='') as file:
        writer = csv.writer(file, delimiter='|')
        writer.writerows(data_list)
    filename = 'predict_' + datetime.datetime.now().strftime('%d-%m_%H:%M') + '.csv'
    print('Uploading to bucket...')
    bucket.upload_file('./predict.csv', filename)
    response_queue.send_message(MessageBody= filename)
    os.remove('./request.csv')
    os.remove('./predict.csv')

